function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image-preview').attr('src', e.target.result);
            
            $('#source-container').attr('backgroundImage', e.target.result);
            storeTheImage(); 
        }
        reader.readAsDataURL(input.files[0]);
        
        $('#download').show();
        $('form').hide();
    }
}

$('.file-input').on('change', function() {
    readURL(this);
});

function downloadCanvas(link, canvasId, filename) {
    html2canvas($('#'+canvasId), 
    {
      onrendered: function (canvas) {
        let img = canvas.toDataURL("image/png");
        window.open(img);
      }
    });
    
    
}

$('#download').on('click', function(){
    downloadCanvas(this, 'overlayed','profilepic.png');
});

$(document).ready(function() {
    $("#png2").draggable({ containment: "#draggable" });
    $('#download').hide();
});